# proto

[![Build Status](https://drone.gitea.com/api/badges/gitea/actions-proto-def/status.svg)](https://drone.gitea.com/gitea/actions-proto-def)

Protocol Buffer schema for Gitea Actions.

## Install

install require command

```sh
make install
```

## Build

```sh
make build
```
